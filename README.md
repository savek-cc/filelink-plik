# Filelink for Plik

A MailExtension for Thunderbird (68+) that uploads large attachments to [Plik](https://plik.root.gg) (or your own instance) and generates a link you can send by mail instead of the file.

Information for

* [Users](#user-content-user-guide)
* [Developers](#user-content-developer-guide)

## Requirements

* Thunderbird: 68.2.1 or newer

## User guide

### Installation

[__Filelink for Plik__](https://addons.thunderbird.net/de/thunderbird/addon/filelink-for-plik/) is available via Thunderbird's Add-on repository.

1. Install it directly from the Add-ons management within Thunderbird.
1. Go to Settings -> Attachments -> Sending and add __Plik__.

### Known issues

#### Microsoft Edge & Smart Screen

Download URLs on [https://plik.root.gg](https://plik.root.gg) are marked as insecure in a quite dramatic way by SmartScreen in Microsoft Edge. So if you send a link to someone who uses Edge they might be irritated by the message.

##### Workarounds

* Set up your own Plik instance with a different host name.
* Tell the receivers of your mail to use a different browser.
* Guide the receiver through the process of using a link that SmartScreen doesn't like.

#### Attaching files from SMB shares

If you attach a file from a SMB share, it's uploaded to the cloud and the share
link is inserted into your mail, but still the file is attached to the message.
This is not a bug in __Filelink for Plik__ but a [known bug in
Thunderbird](https://bugzilla.mozilla.org/show_bug.cgi?id=793118). There is
nothing I can do about it in __Filelink for Plik__, sorry.

**Workaround:** Copy the file from the SMB share to your local disk before attaching it.

## Developer guide

The project lives on GitLab: <https://gitlab.com/joendres/filelink-plik>. If
you'd like to contribute to the project, help with testing on different
platforms, have a feature suggestion or any other comment, just contact
[me](@joendres).

### Dev resources

* Plik [API documentation](https://github.com/root-gg/plik/blob/master/documentation/api.md)
* [Thunderbird WebExtension
  APIs](https://thunderbird-webextensions.readthedocs.io/en/latest/index.html)
* [Example extensions for Thunderbird WebExtensions
  APIs](https://github.com/thundernest/sample-extensions)
* [What you need to know about making add-ons for
  Thunderbird](https://developer.thunderbird.net/add-ons/), not complete at all.
* [Getting started with
  web-ext](https://extensionworkshop.com/documentation/develop/getting-started-with-web-ext)
  If you are developing WebExtensions, you want to use this tool. For debugging
  just set the ```firefox``` config option to your thunderbird binary.

## Contributors

* [Johannes Endres](@joendres), initial implementation, maintainer
